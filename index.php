<?php
//Incluimos el archivo Model.php para contar con la clase model
include "Model.php";
include "Libro.php";
include "Autor.php";
include "sub.php";

//Creamos un objeto de la clase model
$libro = new Libro;
$autor = new Autor;
$sub = new sub;

//Obetenemos todos los libros de la base de datos
$libros  = $libro->all();
$delete =$libro->delete(5);
$whe=$libro->where("id", 3);
$orden =$libro->order("marca");
$editar =$libro->update(3, "Frontier" );
//$libro->insertar("id, marca, precio, detalles", "NULL, 'Suburban', 2588, '5 personas, transmision automatica' " );
//$libro->insertar("id, marca, precio, detalles", "NULL, 'Jeep', 3250, '5 personas, transmision estandar' " );

 
//-----------------------------------------------------------
$autores = $autor->all();
$edi =$autor->update(1, "Volvo" );
$borrar =$autor->delete(5);
$ordenar =$autor->order("marca");
$wher=$autor->where("id", 2);
//$autor->insertar("id, marca, precio, caracteristicas", "NULL, 'Dodge', 850, '5 personas, transmision estandar' " );


//------------------------------------------------------------

$su = $sub->all();
$edi1 =$sub->update(22, "BMW" );
$delet =$sub->delete(24);
$orde =$sub->order("marca");
$wherer=$sub->where("id", 2);
//$sub->insertar("id, marca, precio, caracteristicas", "NULL, 'SUZUKI', 288, '5 personas, transmision estandar' " );

 
?>


<!-- Dibujamos la tabla con los datos de los libros -->
<h3>CAMIONETAS</h3>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Descripcion</th>
    
    </tr>
  </thead>
  <tbody>
  <?php foreach($libros as $libro):?>
    <tr>
      <?php foreach ($libro as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>
<!-- autores -->
<h3>COCHES</h3>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Descripción</th>
      
    </tr>
  </thead>
  <tbody>
  <?php foreach($autores as $autor):?>
    <tr>
      <?php foreach ($autor as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>

<h3>SUB</h3>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Descripción</th>
      
    </tr>
  </thead>
  <tbody>
  <?php foreach($su as $sub):?>
    <tr>
      <?php foreach ($sub as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>

<h3>CAMPO SELECCIONADO "CAMIONETAS"</h3>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Descripción</th>
      
    </tr>
  </thead>
  <tbody>
  <?php foreach($whe as $libro):?>
    <tr>
      <?php foreach ($libro as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>
<h3>CAMPO SELECCIONADO "COCHES"</h3>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Descripción</th>
      
    </tr>
  </thead>
  <tbody>
  <?php foreach($wher as $autor):?>
    <tr>
      <?php foreach ($autor as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>
<h3>CAMPO SELECCIONADO "SUB"</h3>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Descripción</th>
      
    </tr>
  </thead>
  <tbody>
  <?php foreach($wherer as $sub):?>
    <tr>
      <?php foreach ($sub as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>

<h3>TABLA CAMIONETAS ORDENADA</h3>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Descripción</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($orden as $libro):?>
    <tr>
      <?php foreach ($libro as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>

<h3>TABLA COCHES ORDENADA </h3>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Descripción</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($ordenar as $autor):?>
    <tr>
      <?php foreach ($autor as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>

<h3>TABLA SUB ORDENADA </h3>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nombre</th>
      <th>Precio</th>
      <th>Descripción</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($orde as $sub):?>
    <tr>
      <?php foreach ($sub as $value):?>
        <td><?=$value?></td>
      <?php endforeach;?>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>


